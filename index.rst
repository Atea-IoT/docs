.. LoRaWAN Hackathon documentation master file, created by
   sphinx-quickstart on Tue Oct 31 10:08:21 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LoRaWAN Hackathon's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Development boards:
   

   Arduino <arduino>
   The Lion board <lion>
   LoPy <lopy>
   Raspberry Pi <rpi>


.. toctree::
   :maxdepth: 2
   :caption: Sensors:

   Sensors <sensors>

.. toctree::
   :maxdepth: 2
   :caption: The Servers
   
   Loriot <loriot>
   Cayenne <cayenne>
   IBM Watson IOT Platform <ibm>
   ThingSpeak <thingspeak>



