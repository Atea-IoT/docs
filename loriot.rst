Loriot
=============================================


Loriot is a LoRaWAN Network Server.

Get started
-----------

Browse to http://www.loriot.io. 

Create a free account on the site.

This will give you access to register 10 devices and 1 gateway. 